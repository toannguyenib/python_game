#!/bin/bash

_now=$(date +"%Y-%m-%d")
_file1="/home/mycode/stock_data/hnx_eod/hnx_eod_$_now.csv"
_file2="/home/mycode/stock_data/hsx_eod/hsx_eod_$_now.csv"
_file3="/home/mycode/stock_data/hnx_adjusted/hnx_adjusted_$_now.csv"
_file4="/home/mycode/stock_data/hsx_adjusted/hsx_adjusted_$_now.csv"

./atem64 -o "$_file1" --date-from=$_now -n -F , '/home/coop/Dropbox/MetaData/EOD_HNX'

./atem64 -o "$_file2" --date-from=$_now -n -F , '/home/coop/Dropbox/MetaData/EOD_HOSE'

./atem64 -o "$_file3" --date-from=$_now -n -F , '/home/coop/Dropbox/MetaData/EOD_Adjusted Data_HNX'

./atem64 -o "$_file4" --date-from=$_now -n -F , '/home/coop/Dropbox/MetaData/EOD_Adjusted Data_HOSE'

_cmd1="import pysql; pysql.import_price('$_file1','unadjusted')"
_cmd2="import pysql; pysql.import_price('$_file2','unadjusted')"
_cmd3="import pysql; pysql.import_price('$_file3','adjusted')"
_cmd4="import pysql; pysql.import_price('$_file4','adjusted')"

python -c cmd1
python -c cmd2
python -c cmd3
python -c cmd4

