import MySQLdb as mdb
import pandas as pd
#import sqlite3 as lite
### import price from csv file into MySQL database
### syntax import_price(csvfile to import, adj takes one of 2 options: adjusted or unadjusted)
def import_price(csvfile, adj):
	sf = pd.read_csv(csvfile, delim_whitespace=False, names=['symbol', 'date', 'time', 'open', 'high', 'low', 'close', 'volume', 'openint'], header=0)
	conn = mdb.connect('localhost', 'root', 'deltaMASTER', 'stock-db')
	# conn = lite.connect('stock')	
	ticker, date, time, open, high, low, close, volume, openint = sf['symbol'], sf['date'], sf['time'], sf['open'], sf['high'], sf['low'], sf['close'], sf['volume'], sf['openint']    	
	for i in range(len(ticker)):
		content_string =  "'" + ticker[i] + "','" + str(date[i]) + "','" + str(time[i]) + "','" + str(open[i]) + "','" + str(high[i]) + "','" + str(low[i]) + "','" + str(close[i]) + "','" + str(volume[i]) + "','" + str(openint[i]) + "'"
		query = "INSERT INTO " + adj + "(ticker, date, time, open, high, low, close, volume, openint) VALUES(" + content_string + ")"		
		with conn:
			cur = conn.cursor()
			cur.execute(query)
			conn.commit()
			print query
	conn.close()
		
		
