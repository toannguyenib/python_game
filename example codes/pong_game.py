# Implementation of classic arcade game Pong

import simplegui
import random

# initialize globals - pos and vel encode vertical info for paddles
WIDTH = 600
HEIGHT = 400       
BALL_RADIUS = 20
PAD_WIDTH = 8
PAD_HEIGHT = 80
HALF_PAD_WIDTH = PAD_WIDTH / 2
HALF_PAD_HEIGHT = PAD_HEIGHT / 2
LEFT = False
RIGHT = True

ball_pos = [0, 0]
ball_vel = [0, 0]
paddle1_pos = [PAD_WIDTH, HEIGHT/2 - PAD_HEIGHT/2]
paddle2_pos = [WIDTH - PAD_WIDTH, HEIGHT/2 - PAD_HEIGHT/2]
paddle1_vel = [0, 0]
paddle2_vel = [0, 0]


# initialize ball_pos and ball_vel for new bal in middle of table
# if direction is RIGHT, the ball's velocity is upper right, else upper left
def spawn_ball(direction):
    global ball_pos, ball_vel # these are vectors stored as lists
    ball_pos = [WIDTH / 2, HEIGHT / 2]
    # ball_vel = [5, 5]
    ball_vel[0] = random.randrange(120,240) / 60
    ball_vel[1] = random.randrange(60,180) / 60
# define event handlers
def new_game():
    global paddle1_pos, paddle2_pos, paddle1_vel, paddle2_vel  # these are numbers
    global score1, score2  # these are ints
    
    spawn_ball("RIGHT")
    

# def collide_side(pos_x):
#        return ((pos_x - BALL_RADIUS) <= PAD_WIDTH) or ((pos_x + BALL_RADIUS) >= (WIDTH - PAD_WIDTH))
    
# def collide_up_down(pos_y):
#        return ((pos_y <= BALL_RADIUS) or (pos_y >= HEIGHT - BALL_RADIUS))

def draw(canvas):
    global score1, score2, paddle1_pos, paddle2_pos, ball_pos, ball_vel
 
        
    # draw mid line and gutters
    canvas.draw_line([WIDTH / 2, 0],[WIDTH / 2, HEIGHT], 1, "White")
    canvas.draw_line([PAD_WIDTH, 0],[PAD_WIDTH, HEIGHT], 1, "White")
    canvas.draw_line([WIDTH - PAD_WIDTH, 0],[WIDTH - PAD_WIDTH, HEIGHT], 1, "White")
        
    # update ball
       
    ball_pos[0] +=ball_vel[0]
    ball_pos[1] +=ball_vel[1]
        
    # if it hits walls, move backward
    if (ball_pos[0] - BALL_RADIUS <= PAD_WIDTH) or (ball_pos[0] + BALL_RADIUS >= WIDTH - PAD_WIDTH):
        ball_vel[0] = -ball_vel[0]
    elif (ball_pos[1] - BALL_RADIUS <= 0) or (ball_pos[1] + BALL_RADIUS >= HEIGHT):
        ball_vel[1] = -ball_vel[1]
    
    # draw ball
    canvas.draw_circle(ball_pos, BALL_RADIUS, 1, "Red", "White")
    
    # update paddle's vertical position, keep paddle on the screen
    # handle down move
    if (paddle1_pos[1] < HEIGHT - PAD_HEIGHT):
        paddle1_pos[1] += paddle1_vel[1]
    elif (paddle1_pos[1] >= HEIGHT - PAD_HEIGHT):
        paddle1_pos[1] = HEIGHT - PAD_HEIGHT
        
    if (paddle2_pos[1] < HEIGHT - PAD_HEIGHT):     
        paddle2_pos[1] += paddle2_vel[1]
    elif (paddle2_pos[1] >= HEIGHT - PAD_HEIGHT):    
        paddle2_pos[1] = HEIGHT - PAD_HEIGHT
        
    # handle up move
    if (paddle1_pos[1] > 0):
        paddle1_pos[1] += paddle1_vel[1]
    elif (paddle1_pos[1] <= 0):
        paddle1_pos[1] = 0
        
    if (paddle2_pos[1] > 0):     
        paddle2_pos[1] += paddle2_vel[1]
    elif (paddle2_pos[1] <= 0):    
        paddle2_pos[1] = 0
    
    # draw paddles
    canvas.draw_polygon([(0, paddle1_pos[1]), (0, paddle1_pos[1] + PAD_HEIGHT), (paddle1_pos[0], paddle1_pos[1] + PAD_HEIGHT), paddle1_pos], 1, "Blue", "Blue")
    canvas.draw_polygon([(WIDTH, paddle2_pos[1]), (WIDTH, paddle2_pos[1] + PAD_HEIGHT), (paddle2_pos[0], paddle2_pos[1] + PAD_HEIGHT), paddle2_pos], 1, "Red", "Red")
    
    # draw scores
        
def keydown(key):
    global paddle1_vel, paddle2_vel
    # paddle1 down
    if (paddle1_pos[1] < HEIGHT - PAD_HEIGHT):
        if key == simplegui.KEY_MAP['s']:
            paddle1_vel[1] = 2
    elif (paddle1_pos[1] >= HEIGHT - PAD_HEIGHT):
        paddle1_vel[1] = 0    
    
    # paddle2 down
    if key == simplegui.KEY_MAP['down']:
            paddle2_vel[1] = 2
    elif (paddle1_pos[1] >= HEIGHT- PAD_HEIGHT):
        paddle2_vel[1] = 0
    
    
   
def keyup(key):
    global paddle1_vel, paddle2_vel
    # paddle1 up
    if (paddle1_pos[1] > 0):
        if key == simplegui.KEY_MAP['w']:
            paddle1_vel[1] = -2
    elif (paddle1_pos[1] <= 0):
        paddle1_vel[1] = 0
            
    # paddle2
    if (paddle2_pos[1] > 0):
        if key == simplegui.KEY_MAP['up']:
            paddle2_vel[1] = -2
    elif (paddle2_pos[1] <= 0):
        paddle2_vel[1] = 0
    

# create frame
frame = simplegui.create_frame("Pong", WIDTH, HEIGHT)
frame.set_draw_handler(draw)
frame.set_keydown_handler(keydown)
frame.set_keyup_handler(keyup)


# start frame
new_game()
frame.start()

