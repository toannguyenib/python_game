# the "Reset" function not yet work properly, and the game immediately start

# template for "Stopwatch: The Game"
import simplegui
import math

# define global variables
interval = 100
time_ori = 0
position = [50, 50]
time = ""
stop_num = 0
stop_right = 0
result = "0 / 0"
running = True

# define helper function format that converts time
# in tenths of seconds into formatted string A:BC.D
def format(t):
    min = str(int(t/600))    
    sec_temp = int((t%600)/10)
    if sec_temp < 10:
        sec = str(0)+str(sec_temp)
    else:
        sec = str(sec_temp)
    tic = str((t%600)%10)
    return min+":"+sec+"."+tic

# define event handlers for buttons; "Start", "Stop", "Reset"
def start():
    global running
    timer.start()
    running = True

def stop():
    global stop_num, stop_right, result, running
    timer.stop()
    if running == True:
        stop_num += 1
        if time[5] == '0':
            stop_right += 1
        result = str(stop_right) + " / " + str(stop_num)
    running = False
    
def reset():
    global time_ori, time, result
    result = "0 / 0"
    time_ori = 0
    time = format(time_ori)
    running = False
    
    

# define event handler for timer with 0.1 sec interval
def timer_handler():
    global time, time_ori
    time_ori += 1
    time = format(time_ori)


# define draw handler
def draw(canvas):
    canvas.draw_text(time, position, 20, "White")
    canvas.draw_text(result,(150, 20), 15, "White")
    
# create frame
frame = simplegui.create_frame("watch",300, 200)
frame.add_button("Start", start, 100)
frame.add_button("Stop", stop, 100)
frame.add_button("Reset", reset, 100)
timer = simplegui.create_timer(interval, timer_handler)



# register event handlers
frame.set_draw_handler(draw)

# start frame
frame.start()
timer.start()


# Please remember to review the grading rubric
