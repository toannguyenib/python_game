import MySQLdb as mdb
import pandas as pd
# import sqlite3 as lite
### Read csv file into python
def import_price(csvfile):
	sf = pd.read_csv(csvfile, delim_whitespace=False, names=['symbol', 'date', 'time', 'open', 'high', 'low', 'close', 'volume'], header=0)
	conn = mdb.connect('localhost', 'root', 'deltamaster', 'dt_port')
	#conn = lite.connect('stock')	
	ticker, date, time, open, high, low, close, volume = sf['symbol'], sf['date'], sf['time'], sf['open'], sf['high'], sf['low'], sf['close'], sf['volume']    	
	for i in range(len(ticker)):	
		content_string =  "'" + str(ticker[i]) + "','" + str(date[i]) + "','" + str(time[i]) + "','" + str(open[i]) + "','" + str(high[i]) + "','" + str(low[i]) + "','" + str(close[i]) + "','" + str(volume[i]) + "'"
		query = "INSERT INTO stock_quote(ticker_id, q_date, q_time, q_open, q_high, q_low, q_close, q_volume) VALUES(" + content_string + ")"		
		with conn:
			cur = conn.cursor()
			cur.execute(query)
			conn.commit()
			print query
	conn.close()
