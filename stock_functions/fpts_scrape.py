from bs4 import BeautifulSoup
import requests
from datetime import datetime
from datetime import date as dt 
import MySQLdb as db

# http://priceboard.fpts.com.vn/HA4/?s=32&language=VN&r=0.2021143
# scrape price data and save in a database; date in the s_type = ['intra', 'eod'];
# date in form of 'dd/mm/yyyy'
def scrape(s_type, date):
	
	conn = db.connect(host="cerp.deltatech.vn", user="finance", passwd="deltaMASTER", db="stock-db")
	if s_type == 'intra':
		url1 = 'http://priceboard.fpts.com.vn/HA4/?s=32'
		url2 = 'http://priceboard.fpts.com.vn/ho4/?s=33'
		
		for url in [url1, url2]:
			r = requests.get(url)
			data = r.text
			soup = BeautifulSoup(data)
			price_table = soup.find("table", id="FPTSQuote")
			price_rows = price_table.find_all("tr", class_ = "row")
			s_date = dt.today()
			p_date = str(s_date.year) + '-' + str(s_date.month) + '-' + str(s_date.day)
			p_time = "00:00:00"
			for row in price_rows:
				prices = row.find_all("td")
				ticker = prices[0].text.encode("ascii", "ignore")
				p_ceiling = prices[1].text.encode("ascii", "ignore")
				p_floor = prices[2].text.encode("ascii", "ignore")
				p_ref = prices[3].text.encode("ascii", "ignore")
				p_volume = prices[19].text.encode("ascii", "ignore")		
				p_close = prices[10].text.encode("ascii", "ignore")		
				p_open = prices[21].text.encode("ascii", "ignore")
				p_high = prices[22].text.encode("ascii", "ignore")
				p_low = prices[23].text.encode("ascii", "ignore")	
				if p_close=='' and p_open=='' and p_high=='' and p_low=='':
					p_close = p_ref		
					p_open = p_ref
					p_high = p_ref
					p_low = p_ref
				
				content_string =  "'" + ticker + "','" + p_date + "','" + p_time + "','" + p_open + "','" + p_high + "','" + p_low + "','" + p_close + "','" + p_volume + "','" + "0" + "'"
				query = "INSERT INTO unadjusted_2(ticker, date, time, open, high, low, close, volume, openint) VALUES(" + content_string + ")"		
				with conn:
					cur = conn.cursor()
					cur.execute(query)
					conn.commit()
					print query
		conn.close()
	elif s_type == 'eod':
		#url = 'http://priceboard.fpts.com.vn/user/stock/lich-su/?a=2&c=1&s=&t=0&d='06/10/2014&b=06/09/2014&e=06/10/2014&p=1
		
		s_date = datetime.strptime(date,'%d/%m/%Y')
		p_date = str(s_date.year) + '-' + str(s_date.month) + '-' + str(s_date.day)
		p_time = '00:00:00'
		exchanges = ['1','2']
		for e in exchanges:
			url_stock_1 = 'http://priceboard.fpts.com.vn/user/stock/lich-su/?a=1&c=' + e + '&s=&t=2&d=' + date + '&p=1'
			url_stock_2 = 'http://priceboard.fpts.com.vn/user/stock/lich-su/?a=1&c=' + e + '&s=&t=2&d=' + date + '&p=2'
			url_fcert_1 = 'http://priceboard.fpts.com.vn/user/stock/lich-su/?a=1&c=' + e + '&s=&t=3&d=' + date + '&p=1'
			
			if e == '1':
			    row_class = "Status"
			elif e == '2':
			    row_class = "Status0"
			    
			for url in [url_stock_1, url_stock_2, url_fcert_1]:
				r = requests.get(url)
				data = r.text
				soup = BeautifulSoup(data)
				price_table = soup.find("table", id="FPTSQuote")
				price_rows = price_table.find_all("tr", class_= row_class)
				for row in price_rows:
					prices = row.find_all("td", class_="cColumnData")
					
					if prices[1] == '':
						pass
					else:
						ticker = prices[1].text.encode('ascii','ignore')
						
						if prices[14].text == '':
							p_volume = 0
						else:
							p_volume = prices[14].text.encode('ascii','ignore').replace(',','')
					
						if prices[5].text == '':
							p_open = prices[2].text.encode('ascii','ignore')
						else:
							p_open = prices[5].text.encode('ascii','ignore')
						if prices[8].text == '':
							p_low = prices[2].text.encode('ascii','ignore')
						else:
							p_low = prices[8].text.encode('ascii','ignore')
						
						if prices[7].text == '':
							p_high = prices[2].text.encode('ascii','ignore')
						else:
							p_high = prices[7].text.encode('ascii','ignore')
						
						if prices[6].text == '':
							p_close = prices[2].text.encode('ascii','ignore')
						else:
							p_close = prices[6].text.encode('ascii','ignore')
					
						content_string =  "'" + ticker + "','" + p_date + "','" + p_time + "','" + p_open + "','" + p_high + "','" + p_low + "','" + p_close + "','" + p_volume + "','" + "0" + "'"
						query = "INSERT INTO unadjusted_2(ticker, date, time, open, high, low, close, volume, openint) VALUES(" + content_string + ")"		
						with conn:
							cur = conn.cursor()
							cur.execute(query)
							conn.commit()
							print query
		conn.close()
