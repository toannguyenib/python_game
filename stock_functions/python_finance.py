# Notes for learning python for finance:

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Read csv file into python
def read_price(csvfile):
	file = csvfile
	df = pd.read_csv(file)
	price = df['close']
	return price


# Calculate daily return:
	# calculate 1 day return:
def st_ret(price, lag):
	interval = lag
	close = price
	
	price_num = price[interval-1:]
	price_deno = price[0:len(price)-interval]
	
	ret = [np.log(float(ai)/bi) for ai, bi in zip(price_num, price_deno)]
	
	return ret
	
	


