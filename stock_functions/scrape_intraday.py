import dateutil.parser as parser
import datetime as dt
import pandas as pd
import urllib2
import numpy as np

def trading():
	start_time_1 = parser.parse('09:00:00')
	stop_time_1 = parser.parse('11:30:00')	
	start_time_2 = parser.parse('13:00:00')
	stop_time_2 = parser.parse('15:00:00')
	now = dt.datetime.now()
	hour, minute, second = str(now.hour), str(now.minute), str(now.second)
	time_string = hour+':'+minute+':'+second
	now_time = parser.parse(time_string)
	if ((now_time > start_time_1) and (now_time < stop_time_1)):
		return True
	elif ((now_time > stop_time_1) and (now_time < start_time_2)):
		return False
	elif ((now_time > start_time_2) and (now_time < stop_time_2)): 
		return True
	else:
		return False

def scrape_trim(market):
	if market == 'hnx':
		url = 'http://banggia.cafef.vn/stockhandler.ashx?center=2'
	if market == 'hsx':
		url = 'http://banggia.cafef.vn/stockhandler.ashx?center=1'
	response = urllib2.urlopen(url)
	data = response.read()
	info = data.lstrip('[{').rstrip('}]') 
	### turn data into a list by separating item by ('},{')
	info = info.split('},{')
	return info

def scrape(market):
	if trading() == False:
		print 'The market is NOT trading'
		pass
	else:	
		### announce the status to the console		
		status = 'The market is trading now. You are scraping data from ' + market + '\n '
		status = status + 'The program will run continuously until the market stops trading. If you want to stop it, hit Ctrl + C to terminate'	
		print status

		### calculate date and time string to use as the name of the output file 
		now = dt.datetime.now()
		now_date = str(now.year) + '-' + str(now.month) + '-' + str(now.day)	
		file_name = now_date + '-' + market + '.csv'
	
		### if the output file is empty, open it to write the header and close it when finished	
		of = open(file_name,'w+b')
		source = of.read()
		if len(source) == 0:
			header = 'date, time, ticker, ref, ceiling, floor, b_pr_3, b_vol_3, b_pr_2, b_vol_2, b_pr_1, b_vol_1, ch, m_pr, m_vol, total_vol, s_pr_1, s_vol_1, 2_pr_2, 2_vol_2, s_pr_3, s_vol_1, emp_1, high, low, f_buy, y, z' + '\n'			
			of.write(header)
			#of.close()		
		else:
			### if the file is not empty, open the file to append quote data
			of = open(file_name,'a')
	
		scrape_data = ''
		
		### start the scraping loop when the market is trading
		condition = trading()		
		while condition==True: 
			### get current date & time as string to write to the output file		
			scrape_date_time = dt.datetime.now()
			scrape_time = str(scrape_date_time.hour) + ':' + str(scrape_date_time.minute) + ':' + str(scrape_date_time.second)
			scrape_date = str(scrape_date_time.year) + '-' + str(scrape_date_time.month) + '-' + str(scrape_date_time.day)		
		
			### get trimmed data by calling scrape_trim function
			info = scrape_trim(market)
			if scrape_data == '':		
				scrape_data = info		
			else:
				### compare each line of scrape_data and info, and only write to output file line info 
				### which are different from scrape_data
				for i in range(len(scrape_data)-1):
					if scrape_data[i] == info[i]:
						pass
					else:				
						tick_info = info[i].split(',')
						line_info = '\n' + scrape_date + ',' + scrape_time + ','	
						for j in range(len(tick_info)-1):
							price_info = tick_info[j].split(':')
							line_info = line_info + str(price_info[1]) + ','
						of.write(line_info)
						scrape_data = info
						condition = trading()
		of.close()

def get_ticker_list():
	import MySQLdb as db
	conn = db.connect(host="192.168.1.3", user="delta", passwd="deltamaster", db="stock-db")
	query = "SELECT DISTINCT `ticker` FROM unadjusted;"
	cur = conn.cursor()
	cur.execute(query)
	result = cur.fetchall()
	ticker_list = []
	for tk in result:
		ticker_list.append(tk)
	return ticker_list

### calculate ohlc and write it to file 
def EOD(filename):
	### read in the intraday - scraped file	
	sf = pd.read_csv(filename, sep=",", names=['date', 'time', 'ticker', 'ref', 'ceiling', 'floor', 'b_pr_3', 'b_vol_3', 'b_pr_2', 'b_vol_2', 'b_pr_1', 'b_vol_1', 'ch', 'm_pr', 'm_vol', 'total_vol', 's_pr_1', 's_vol_1', 's_pr_2', 's_vol_2', 's_pr_3', 's_vol_3', 'emp_1', 'high', 'low', 'f_buy', 'y', 'z'], header=1, index_col=False)
	
	### read in ticker list
	tk_list = get_ticker_list()
	of = open('ohlc.csv', 'w')
	ticker = sf['ticker'].tolist()	
	for line in tk_list:
		tick_name = line[0]
		if tick_name not in ticker:
			pass
		else: 		
			ticker_info = sf[sf['ticker'] == tick_name] 		
			matched_price = ticker_info['m_pr'].tolist()
			time = ticker_info['time'].tolist()
			p_high = max(matched_price)
			p_low = min(matched_price)		
			row_open = ticker_info[ticker_info['time'] == min(time)]
			row_close = ticker_info[ticker_info['time'] == max(time)]
			p_vol = row_close['total_vol'].tolist()[0]
			p_open = row_open['m_pr'].tolist()[0]
			p_close = row_close['m_pr'].tolist()[0]
			line_to_write = tick_name + ',' + str(row_close['date'].tolist()[0]) + ',00:00:00,' + str(p_open) + ',' + str(p_high) + ',' + str(p_low) + ',' + str(p_close) + ',' + str(p_vol) + ',0' + '\n'   
			of.write(line_to_write)
	of.close()

### calculate ohlc and write it to file 
def EOD_process(filename):
	### read in the intraday - scraped file	
	sf = pd.read_csv(filename, sep=",", names=['date', 'time', 'ticker', 'ref', 'ceiling', 'floor', 'b_pr_3', 'b_vol_3', 'b_pr_2', 'b_vol_2', 'b_pr_1', 'b_vol_1', 'ch', 'm_pr', 'm_vol', 'total_vol', 's_pr_1', 's_vol_1', 's_pr_2', 's_vol_2', 's_pr_3', 's_vol_3', 'emp_1', 'high', 'low', 'f_buy', 'y', 'z'], header=1, index_col=False)
	### screen out row with matched volume = 0
	short_sf = sf[sf.m_vol != 0]
	### group them by ticker and 'describe' the data of each ticker to calculate min, max 
	data = short_sf.groupby(['ticker']).describe()
	vol = short_sf.groupby(['ticker']).sum()
	### get openning price
	open_price_list = short_sf.groupby(['ticker']).first()
	closing_price_list = short_sf.groupby(['ticker']).last()
	###
	eod_data = {}	
	ofile = 'ohcl-'+filename[51:]	
	of = open(ofile, 'w')	
	for ticker in open_price_list.index:
		eod_data['ticker'] = ticker
		eod_data['open'] = open_price_list.loc[ticker,'m_pr']
		eod_data['close'] = closing_price_list.loc[ticker,'m_pr']
		eod_data['high'] = data.loc[ticker,'max']['m_pr']
		eod_data['low'] = data.loc[ticker,'min']['m_pr']
		eod_data['vol'] = vol.loc[ticker,'m_vol']
		eod_data['openint'] = 0
		eod_data['date'] = open_price_list.loc[ticker,'date']
		line_to_write = eod_data['ticker'] + ',' + str(eod_data['date']) + ',' + '00:00:00' + ',' + str(eod_data['open']) + ',' + str(eod_data['high']) + ',' + str(eod_data['low']) + ',' + str(eod_data['close']) + ',' + str(eod_data['vol']) + ',' + str(eod_data['openint']) + '\n'
		of.write(line_to_write)
	of.close()			
		
	
