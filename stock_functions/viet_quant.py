import numpy as np
from scipy import stats
import pandas as pd
import MySQLdb as db
import pylab as pl
import matplotlib.pyplot as plt

# to get a list of tickers from a CSV file extracted from MetaStock Data using atem
def get_ticker():
	conn = db.connect('cerp.deltatech.vn', 'delta', 'deltaMASTER', 'stock-db')
	query = "SELECT DISTINCT ticker FROM adjusted"
	cur = conn.cursor()
	cur.execute(query)
	result = cur.fetchall()
	tk_list = []
	for row in result:
		tk_list.append(row[0])
	ret = [tk_list][0]
	conn.close()
	return ret	

def split_file(csvfile):
	sf = pd.read_csv(csvfile, delim_whitespace=True, names=['symbol', 'date', 'time', 'open', 'high', 'low', 'close', 'volume', 'openint'], header=0)
	ticker = sf['symbol']
	date = sf['date']
	price= sf['close']
	
	tk_list = open('ticker_list_hsx.txt', 'r').readlines()
	
	for line in tk_list:
		tk = line[0:len(line)-1]
		filename = '/home/toannguyen/mycode/python_learning/hsx/'+tk+'.csv'
		of = open(filename, 'w')		
		for i in range(0, len(ticker)-1, 1):
			if ticker[i] == tk:
				line_data = ticker[i]+','+date[i]+','+str(price[i]) + '\n'
				of.write(line_data)			
			else:
				pass
	of.close()
	
def stock_info(price, nper, lag):
	a = np.array(price[lag:])
	b = np.array(price[0:len(price)-lag])
	ret_interval = np.log(b/a, dtype='float64')
	info_length = min(nper, len(a), len(b))
	ret_nper = ret_interval[0:info_length]	
	n, min_max, mean, var, skew, kurt = stats.describe(ret_nper)
	st_dev = np.std(ret_nper)
	ret = [n, min_max[0], min_max[1], mean, var, st_dev, skew, kurt]
	return ret 
	
def stock_sum(tick_name, nper, lag):
	prices = read_price(tick_name)[1]	
	
	# info_length = min(nper,len(sf))
	if len(prices) <= nper:	
		price_data = prices
		info_length = len(price_data)
	else:
		price_data = prices[len(prices)-nper:len(prices)]
		info_length = nper
	ret = stock_info(price_data, info_length, lag)
	of = open('stock_summary_dec_12_2014.csv', 'a')
	of.write(tick_name + ',')
	for s in range(0, len(ret), 1):
		of.write(str(ret[s]) + ',')
	of.write('\n')
	of.close()

def stock_sum_all(nper, lag):
	tk_list = get_ticker()
	for tk in tk_list:
		stock_sum(tk, nper, lag)		

# stock_ret function take input as price list in the order of oldest in the back, latest in the front
def stock_ret(price, lag):
	a = np.array(price[lag:])
	b = np.array(price[0:len(price)-lag])
	ret_interval = np.log(b/a, dtype='float64')
	return ret_interval

def beta(ticker_name, index, nper, lag):
	# load the right index file	
	if index == 'hnx':	
		index_file_name = '/home/toannguyen/mycode/python_learning/hnx/HNXINDEX.csv'
	elif index == 'hsx':
		index_file_name = '/home/toannguyen/mycode/python_learning/hsx/VNINDEX.csv'
	
	index_file = pd.read_csv(index_file_name, names=['ticker', 'date', 'price'], header=0)
		
	# load the right ticker file
	tk_list_hnx = open('ticker_list.txt','r').readlines()
	tk_list_hsx = open('ticker_list_hsx.txt','r').readlines()
		
	for line in tk_list_hnx:
		if ticker_name == line[0:len(line)-1]: 
			ticker_filename = '/home/toannguyen/mycode/python_learning/hnx/'+ticker_name+'.csv'	
	
	for line in tk_list_hsx:
		if ticker_name == line[0:len(line)-1]:
			ticker_filename = '/home/toannguyen/mycode/python_learning/hsx/'+ticker_name+'.csv'

	print 'ticker_filename is: ', ticker_filename
	
	ticker_file = pd.read_csv(ticker_filename, names=['ticker', 'date', 'price'], header=0)	
	 		
	# read prices of ticker and of index into variables
	index_value = index_file['price']
	ticker_price = ticker_file['price']
		
	index_return = stock_ret(index_value, lag)[::-1]
	ticker_return = stock_ret(ticker_price, lag)[::-1]
	
	beta_length = min(nper, len(ticker_return), len(index_return))
 
	x = index_return[0:beta_length]
	y = ticker_return[0:beta_length]
	
	slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
	ret = [slope, intercept,r_value, r_value**2, 1-r_value**2, p_value, std_err]
	return ret

def beta_mass(nper, lag):
	# Read in ticker list
	tk_list_hnx = open('ticker_list.txt','r').readlines()
	tk_list_hsx = open('ticker_list_hsx.txt','r').readlines()
	# Open output files for writing and write the headings, one file for hnx, another for hsx 	
	of_hnx = open('beta_hnx.csv','w')
	of_hnx.writelines('ticker, slope, intercept,correlation, r_square, firm_risk, p_value, std_err' + '\n')
	of_hsx = open('beta_hsx.csv','w')
	of_hsx.writelines('ticker, slope, intercept,correlation, r_square, firm_risk, p_value, std_err' + '\n')		
	for line in tk_list_hnx:
		ticker_name = line[0:len(line)-1]
		ret1 = str(beta(ticker_name,'hnx', nper, lag))
		ret2 = str(beta(ticker_name, 'hsx', nper, lag))
		of_hnx.writelines(ticker_name + ',' + ret1 + '\n')
		of_hsx.writelines(ticker_name + ',' + ret2 + '\n')		 
	
	for line in tk_list_hsx:
		ticker_name = line[0:len(line)-1]
		ret1 = str(beta(ticker_name,'hnx', nper, lag))
		ret2 = str(beta(ticker_name, 'hsx', nper, lag))
		of_hnx.writelines(ticker_name + ',' + ret1 + '\n')
		of_hsx.writelines(ticker_name + ',' + ret2 + '\n')
	of_hnx.close()
	of_hsx.close()

def port_return(weight, mu):
	if sum(weight) <= 1.0:	
		x = np.matrix(weight)
		mu = np.matrix(mu)
	else:
		print 'Wrong input. Weights do not sum to 1'
		pass				
	return np.dot(x, mu.T)

def port_var(weight, sigma):
	### Calculate portfolio variance given a weight array (1 row x n column), and sigma array (n by n) as arguments 
	x_vec = np.matrix(weight)
	sigma_vec = np.matrix(sigma)
	if sum(weight) != 1 or len(weight) != sigma.shape[0]:
		print 'Invalid input weights do not sum to 1 or wrong shape of sigma'
	else:
		var_temp = (x_vec * sigma_vec) 
		var = var_temp * x_vec.T
	return var
	
### portfolio optimization function from ehrmo dot blogspot dot com 
def markowitz(mu, sigma, mu_p):
 def iif(cond, iftrue=1.0, iffalse=0.0):
  if cond:
   return iftrue
  else:
   return iffalse

 from cvxopt import matrix, solvers
 #solvers.options['show_progress'] = False

 d = len(sigma)

 # P and q determine the objective function to minimize
 # which in cvxopt is defined as $.5 x^T P x + q^T x$
 P = matrix(sigma)
 q = matrix([0.0 for i in range(d)])

 # G and h determine the inequality constraints in the
 # form $G x \leq h$. We write $w_i \geq 0$ as $-1 \times x_i \leq 0$
 # and also add a (superfluous) $x_i \leq 1$ constraint
 G = matrix([
  [ (-1.0)**(1+j%2) * iif(i == j/2) for i in range(d) ]
  for j in range(2*d)
 ]).trans()
 h = matrix([ iif(j % 2) for j in range(2*d) ])

 # A and b determine the equality constraints defined
        # as A x = b
 A = matrix([[1.0 for i in range(d)], mu]).trans()
 b = matrix([ 1.0, float(mu_p) ])

 sol = solvers.qp(P, q, G, h, A, b)

 if sol['status'] != 'optimal':
  raise Exception("Could not solve problem.")

 w = list(sol['x'])
 f = 2.0*sol['primal objective']

 return {'w': w, 'f': f, 'args': (P, q, G, h, A, b), 'result': sol }

#def var_e(ticker, market):
#	var_hnx = stock_sum('HNXINDEX', 500, 5)
#	var_hsx = stock_sum('VNINDEX', 500, 5)
#	var_ticker= stock_sum(ticker, 500, 5)

### return date and price array with price of the latest date at the end of the price array
def read_price(tick_name):
	conn = db.connect('cerp.deltatech.vn', 'delta', 'deltaMASTER', 'stock-db')
	query = "SELECT date, close FROM adjusted WHERE ticker=" + "'" + tick_name+"'"
	cur = conn.cursor()
	cur.execute(query)
	result = cur.fetchall()
	date = []
	price = []
	for row in result:
		date.append(row[0])
		price.append(row[1])
	ret = [date, price]
	conn.close()
	return ret

### calculate the covarian of 2 tickers' return using numpy.cov(stock return, index return)
### it returns an array 2 x 2 array. the [0, 1] element is the normal covariance that we need to take
def covar(ticker1, ticker2, nper, lag):
	
	price1 = read_price(ticker1)[1][::-1]
	price2 = read_price(ticker2)[1][::-1]
	data_length = min(nper, len(price1), len(price2))
	price1_data = price1[0:data_length]
	price2_data = price2[0:data_length]
	ret1 = stock_ret(price1_data, lag)
	ret2 = stock_ret(price2_data, lag)
	ret = np.cov(ret1, ret2)[0, 1]
	return ret

### return the covariance matrix of a portfolio of stock given in tk_list
def cov_arr(tk_list, nper, lag):
	tk_len = len(tk_list)
	sig_vec = np.zeros(tk_len**2)
	sig_vec.shape = (tk_len, tk_len)
	# sig_vec = np.array([[],[]])
	# sig_vec.shape = (tk_len, tk_len)
	for i in range(tk_len):
		for j in range(tk_len):
			ticker_1 = tk_list[i]
			ticker_2 = tk_list[j]		
			sig_vec[i, j] = covar(ticker_1, ticker_2, nper, lag)
	return sig_vec 

### propose the optimum portfolio for a list of tickers using historical data in the database
def optimize_port(tk_list, nper, lag, mu_p):
	## construct the mu_vec	
	mu_vec = []	
	for ticker in tk_list:
		price = read_price(ticker)[1][::-1]
		d_len = min(nper, len(price))
		x = np.array(price[lag:d_len])
		y = np.array(price[0:d_len-lag])		
		stock_ret = np.log(y/x, dtype='float64')
		mu = np.mean(stock_ret)*(252/lag)
		mu_vec.append(mu)
	print 'mu_vec is:' 
	print mu_vec		
	 
	## construct the sigma_vec
	sig_vec = cov_arr(tk_list, nper, lag)
	print 'Covariance matrix is:'
	print sig_vec

	## call markowitz function to find the optimum portfolio
	ret = markowitz(mu_vec, sig_vec, mu_p)
	return ret['w']

def ef_plot(tk_list, nper, lag):
	mu_vec = []
	min_vec	 = []
	max_vec	 = []
	for ticker in tk_list:
		price = read_price(ticker)[1][::-1]
		d_len = min(nper, len(price))
		x = np.array(price[lag:d_len])
		y = np.array(price[0:d_len-lag])		
		stock_ret = np.log(y/x, dtype='float64')
		min_ret_tick, max_ret_tick, mu = np.min(stock_ret)*(252/lag), np.max(stock_ret)*(252/lag), np.mean(stock_ret)*(252/lag)				
		# min_vec.append(min_ret_tick)
		# max_vec.append(max_ret_tick)
		mu_vec.append(mu)					
	
	print 'mu_vec is'
	print mu_vec
		
	# min_ret = np.min(min_vec)
	# max_ret = np.max(max_vec)
	sigma = cov_arr(tk_list, nper,lag)
	print 'sigma_vec is:'
	print sigma

	ret_vec = []
	var_vec = []
	r_p_range = np.arange(0.01, 0.35, 0.02)
	for i in range(len(r_p_range)):
		r_p = r_p_range[i]
		print 'Portfolio expected return is:' + str(r_p)
		w = markowitz(mu_vec, sigma, r_p)['w']
		print 'Optimal portfolio is:'
		print w		
		port_ret = port_return(w, mu_vec)
		ret_vec.append(port_ret)
		port_variance = port_var(w, sigma)
		var_vec.append(port_variance)
	chart = pl.scatter(ret_vec, var_vec)
	pl.show()

def describe(ticker, nper, lag):
	# Read price into variables:
	ticker_info = read_price(ticker) # return a list of prices and dates
	ticker_date = ticker_info[0][::-1] # read date into a separate list and sort it in the order of latest date in front
	ticker_price = ticker_info[1][::-1] # read price into a separate list and sort it in the order of latest date in front
	info_len = min(nper, len(ticker_date))	
	# Calculate weekly cc stock return stock_ret(price, lag)
	ticker_ret = stock_ret(ticker_price[0:info_len], lag)
	data_len = len(ticker_ret)	
	# Describe the data
	ticker_desc = stock_info(ticker_price, info_len, lag)
	ticker_ret_min = str(ticker_desc[1])
	ticker_ret_max = str(ticker_desc[2])
	ticker_ret_mean = str(ticker_desc[3])
	ticker_ret_sd = str(ticker_desc[5])
	ticker_ret_sk = str(ticker_desc[6])
	ticker_ret_kurt = str(ticker_desc[7])
	# Create a figure	
	plt.figure(1)
	# Add subplot 1 to display weekly cc return
	plt.subplot(211)
	plt.plot(ticker_date[0:data_len], ticker_ret)
	plt.plot((min(ticker_date[0:data_len]), max(ticker_date[0:data_len])), (0,0)) # add a horizontal like at 0
	plt.title('Weekly continuously compounded return')
	legend_string = 'Min = ' + ticker_ret_min + '\n'
	legend_string += 'Max = ' + ticker_ret_max + '\n'
	legend_string += 'Mean = ' + ticker_ret_mean + '\n'
	legend_string += 'Sd = ' + ticker_ret_sd + '\n'	
	plt.text(ticker_date[data_len -5], max(ticker_ret) - 0.15, legend_string)
	
	# Add subplot 2 to display the histogram of weekly cc return
	plt.subplot(212)
	plt.hist(ticker_ret, 40, normed=0, facecolor='g', alpha=0.75)
	plt.xlabel('Return levels')
	plt.ylabel('Number of weeks')
	plt.title('Return histogram')
	legend_string_1 = 'Skewness = ' + ticker_ret_sk + '\n'
	legend_string_1 += 'Kurt = ' + ticker_ret_kurt
	plt.text(-.15, 20, legend_string_1)	
	# Show the chart
	plt.show()
